![Alt text](https://esbagenda.lodiusd.net/images/lusdlogolrg.jpg)
# ** LUSD Service: Read Me **

This is a service for Lodi Unified School District (LUSD). It's intended to be a hub for LUSD scripts. *The service has only been tested on Windows 10.

Repo Owner: Jordan Wernette
Email: Jwernette@lodiusd.net, Jordan.Wernette@gmail.com

	
# Script Configuration File


The list of scripts that the service will run is contained in a text file named "script_list.txt". 
This text file will be located in C:\Program Files (x86)\Lusd\Lusd Service... and will be structured:

    Checksum, Script Name,Interval, True || False, Path of Script

'Checksum' is a method of maintaining correct data. Checksum will be equal to the amount entries on a given line within "script_list.txt". For the example above the Checksum
  would be equal to 4 (Note that the checksum isn't considered in the total). Any number other than 4 in the above example will result in the service skipping over the script.
  If a 4 is entered as the Checksum, and some data is missing such as the Path of Script, the service will again skip over that script to prevent run time errors.

'Script Name' can be any string, but should be the name of the script it's running.

'Interval' is measured in milliseconds, therefore an interval of 60000 would mean your script runs once every 60 seconds.

'True || False' will take either 'true' or 'false' here. Entering in true here will result in your script repeating more than once. False will mean after your interval is up your
  script will not run again until the service is restarted.

'Path of Script' will contain the path of the script you wish to run. Each script to be ran by the service must be in a separate folder than the others.

	
# Script Guidelines

	
As mentioned above, each script must be in its own folder separated from other scripts that are ran the service.

To ensure that the service isn't running a script that fails continually, each script must create a text file 'Done.txt' after your script has finished successfully. This text file
 must be located in the same folder as the script.
 
The service will check to see if Done.txt exists within the folder. If it does exist, my script will delete that file and run the script again when the interval is up. If Done.txt is
 not present in the folder, the service will assume the script failed and skip over it.
 
If you have found that your script is not working, fix the script, and wish the service to continue, then you can create a text file 'Reset.txt' and place it in the same folder as the script.
 This will allow the script to become active again and will repeat the script as usual.
 
If you have a script that you do not wish to remove from the script_list.txt config file, but also do not wish to run, you can place a text file 'DoNotRun.txt' in the same folder as the script.
 My service will skip over scripts that have DoNotRun.txt in the same folder.
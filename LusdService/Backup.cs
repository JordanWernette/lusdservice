﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace LusdService
{
    public class Backup
    {
        
        string scriptlistPath = AppDomain.CurrentDomain.BaseDirectory + "/script_list.txt";
        string scriptlistDest = AppDomain.CurrentDomain.BaseDirectory + "/script_list_backup.txt";

        public bool errorOccured=false;
       // public bool backupExists = false; // 

        public void createBackup()
        {
            if (!errorOccured)
            {
                Log.WriteLog("Timers were created without errors. Creating backup of script list...");
                try
                {
                    File.Copy(scriptlistPath, scriptlistDest, true); // Copy contents to backup.
                }
                catch(Exception e)
                {
                    Log.WriteErrorLog("Unable to create backup of file: " + e.Message);
                }
            }
            else if (errorOccured)
            {
                Log.WriteErrorLog("Encountered errors creating timers for scripts. Will not create backup of script list."); // Serious Errors, Tried to revert previously.
            }
        }

        public void revertToBackUp()
        {
#if DEBUG
            //backupExists = true; // Testing backup.
#endif
            if (File.Exists(scriptlistDest))
            {
                File.Copy(scriptlistDest, scriptlistPath, true); // Move contents of back_up to contents of script_list
            }
            else
            {
                Log.WriteErrorLog("Unable to revert to back up, no file found.");
                // Need to handle what to do if there's no back_up and script fails.
            }

        }
    }

}

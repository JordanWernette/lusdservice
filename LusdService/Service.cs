﻿using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.IO;
using System.Timers;



namespace LusdService
{
    public partial class Service : ServiceBase
    {
        public object Library { get; private set; }
        public Service()
        {
            InitializeComponent();
        }

        string scriptListPath = "script_list.txt"; // Location of the scripts text file.
        Backup backUp = new Backup(); // Creating back_up object
        List<string> blackListScript = new List<string>(); // Will not create timers for blacklisted scripts.

        protected override void OnStart(string[] args)
        {
#if DEBUG
            Log.WriteErrorLog("* ****************** Debugging Service has started ******************");
#else
            Log.WriteErrorLog("******************* Service has started *******************");
#endif
            Log.WriteErrorLog("Current Logged in user: " + Currentuser.loggedOn());

            int timer_count = 0;
            string line;
            //int line_count = 0;
            int num; // used to check if interval is int when parsing.

            // Temp list to transfer split items for txt file to properties of an instance 
            List<string> tmp_splitList = new List<string>();
            Dictionary<int, NamedTimer> timer_list = new Dictionary<int, NamedTimer>();
            
            for (int retry = 0; ; retry++)
            {
                int line_count = 0;
                Log.WriteErrorLog("Retry #:" + retry);
                try
                {
                    using (StreamReader file = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + scriptListPath))
                    {
                        while ((line = file.ReadLine()) != null)
                        {
                            int checkSum = -1; // Set to -1 to account for first index being the checksum
                            line = line.Trim(' ', '\t', '\r', '\n');
                            if (line == " " || line == "" || line == "\n" || line == "\r" || line == "\t")
                            {
                                // Do nothing and skip to next line.
                            }
                            else
                            {
                                line_count++;
                                timer_count++;
                                NamedTimer timer = new NamedTimer(timer_count.ToString()); // Initialize timer with number as name
                                string[] split_line;
                                split_line = line.Split(',');

                                foreach (string x in split_line)
                                {
                                    tmp_splitList.Add(x);
                                    checkSum++;
                                }

                                // If checksum is correct and name of script isn't blacklisted
                                if ((int.Parse(tmp_splitList[0])) == checkSum && !blackListScript.Contains(tmp_splitList[4]))
                                {
                                    // Assign properties to each dynamically created timer
                                    timer.name = tmp_splitList[1];
                                    if (int.TryParse(tmp_splitList[2], out num))
                                    {
                                        timer.Interval = Math.Abs(int.Parse(tmp_splitList[2]));
                                    }
                                    else
                                    {
                                        Log.WriteErrorLog("Interval too big of a number.");
                                    }

                                    timer.repeatTimer = bool.Parse(tmp_splitList[3]);       

                                    if (File.Exists(tmp_splitList[4]))      // Verify file exists before assigning it to property
                                    {
                                        timer.scriptPath = tmp_splitList[4];
                                    }
                                    else
                                    {
                                        blackListScript.Add(tmp_splitList[4]);
                                        tmp_splitList.Clear();
                                        throw new Exception();
                                    }

                                    if (tmp_splitList.Count != 0)
                                    {
                                        timer_list.Add(timer_count, timer);
                                        tmp_splitList.Clear();
                                    }

                                }

                                else
                                {
                                    // Continue on to next line, make note of which line failed.
                                    // Clear Temp array
                                    Log.WriteErrorLog("Checksum failed or Script has been blacklisted on line: " + line_count);
                                    tmp_splitList.Clear();
                                    backUp.errorOccured = true; // Do not create a backup.
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    timer_list.Clear();

                    if (retry < 1)
                    {
                        Log.WriteErrorLog("Error trying to read text file: " + e.Message + " Trying to revert to backup.");
                        backUp.revertToBackUp();
                        blackListScript.Clear();
                        continue;
                    }
                    else
                    {
                        Log.WriteErrorLog("Error trying to read text file: " + e.Message + " Already tried to revert to back up... Moving on to next script");
                        backUp.errorOccured = true;
                        continue;
                    }
                }

                break;
            }

            try
            {
                // Going through each timer in the dictionary and starting it
                foreach (KeyValuePair<int, NamedTimer> entry in timer_list)
                {
                    Log.WriteErrorLog("Initializing Timer...");
                    Log.WriteErrorLog("Script Name: " + entry.Value.name);
                    Log.WriteErrorLog("Script Timer Interval: " + entry.Value.Interval);
                    Log.WriteErrorLog("Starting Timer...");
                    if (entry.Value.Interval <= 1000)
                    {
                        Log.WriteErrorLog("Will not start timer for script: " + entry.Value.name + ", either timer interval is too short or no interval given.");
                    }
                    else
                    {
                        entry.Value.Elapsed += (sender, e) => OnTick(sender, e, entry.Value);
                        entry.Value.AutoReset = entry.Value.repeatTimer;
                        entry.Value.Start();
                    }
                }
            }
            catch (Exception e)
            {
                Log.WriteErrorLog("An error occured when trying to read text file, trying to revert to back up to fix the issue... " + e.Message);
                backUp.revertToBackUp();
            }

            backUp.createBackup(); // No errors in creating any timers. Create back_up.
        }

        protected override void OnStop()
        {
            Log.WriteErrorLog("******************* Service has stopped *******************");
            blackListScript.Clear();
        }

        private static void OnTick(object sender, ElapsedEventArgs e, NamedTimer timer)
        {
            Log.WriteErrorLog("Starting script: " + timer.scriptPath);
            timer.runScript();
        }

        public void OnDebug()
        {
            OnStart(null);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Timers;


namespace LusdService
{
    class NamedTimer : Timer
    {
        public string name;
        public string scriptPath;
        public bool repeatTimer;
        public bool scriptFinished; // Scripts will create text file done.txt in their dir. If it's not there scriptFinished = false, and won't run again.

        // Constructors for NamedTimer Class
        public NamedTimer()
        {
            this.name = "No Script Name";
            this.Interval = 1;
            this.scriptPath = "No Script Path";
            this.repeatTimer = true;
            this.scriptFinished = true;
        }

        public NamedTimer(string name)
        {
            this.name = name;
            this.Interval = 1;
            this.scriptPath = "No Script Path";
            this.repeatTimer = true;
            this.scriptFinished = true;
        }

        public NamedTimer(string name, string scriptPath)
        {
            this.name = name;
            this.Interval = 1;
            this.scriptPath = scriptPath;
            this.repeatTimer = true;
            this.scriptFinished = true;
        }

        public NamedTimer(string name, int timer_interval, bool runOnce, string scriptPath)
        {
            this.name = name;
            this.Interval = Math.Abs(timer_interval);
            this.repeatTimer = runOnce;
            this.scriptPath = scriptPath;
            this.scriptFinished = true;
        }

        public void runScript()
        {
            if ((!this.scriptFinished && File.Exists(Path.GetDirectoryName(this.scriptPath) + "/Done.txt")) || File.Exists(Path.GetDirectoryName(this.scriptPath) + "/Reset.txt"))
            {
                if (File.Exists(Path.GetDirectoryName(this.scriptPath) + "/Reset.txt"))
                {
                    File.Delete(Path.GetDirectoryName(this.scriptPath) + "/Reset.txt");
                    this.scriptFinished = true;
                }
                else
                {
                    this.scriptFinished = true;
                }
            }

            if (this.scriptFinished)
            {
                Process script = new Process();
                try
                {
                    script.StartInfo.FileName = scriptPath;
                    script.StartInfo.UseShellExecute = true;
                    script.StartInfo.ErrorDialog = false;
                    script.Start();
                    this.scriptFinished = false; //script finished, move to false to verify the next time it finished.
                    if (File.Exists(Path.GetDirectoryName(this.scriptPath) + "/Done.txt"))
                    {
                        File.Delete(Path.GetDirectoryName(this.scriptPath) + "/Done.txt");
                    }
                }
                catch (Exception e)
                {
                    Log.WriteErrorLog(e.Message + " Cannot start script");
                }
            }
            else
            {
                Log.WriteErrorLog("Will not run script: " + this.scriptPath + "... Script failed last time and there is no file named Reset.txt in scripts directory");
            }
        }
    }
}

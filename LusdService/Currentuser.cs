﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;

namespace LusdService
{
    class Currentuser
    {
        //bool isLoggedOn = false;
        public static string loggedOn()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT Username FROM Win32_ComputerSystem");
            ManagementObjectCollection collection = searcher.Get();
            string user = (string)collection.Cast<ManagementBaseObject>().First()["Username"];
            if (user == "")
            {
                return user = "No user logged in.";
            } 
            else
            {
                return user;
            }
        }
    }
}

﻿namespace LusdService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LusdserviceProcessInstaller1 = new System.ServiceProcess.ServiceProcessInstaller();
            this.LusdserviceInstaller1 = new System.ServiceProcess.ServiceInstaller();
            // 
            // LusdserviceProcessInstaller1
            // 
            this.LusdserviceProcessInstaller1.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.LusdserviceProcessInstaller1.Password = null;
            this.LusdserviceProcessInstaller1.Username = null;
            // 
            // LusdserviceInstaller1
            // 
            this.LusdserviceInstaller1.ServiceName = "LusdService";
            this.LusdserviceInstaller1.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            this.LusdserviceInstaller1.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.serviceInstaller1_AfterInstall);
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.LusdserviceProcessInstaller1,
            this.LusdserviceInstaller1});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller LusdserviceProcessInstaller1;
        private System.ServiceProcess.ServiceInstaller LusdserviceInstaller1;
    }
}